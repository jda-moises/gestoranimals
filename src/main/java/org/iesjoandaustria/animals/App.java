package org.iesjoandaustria.animals;
import org.iesjoandaustria.animals.control.Control;
/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        Control control = new Control();
        control.run();
    }
}
