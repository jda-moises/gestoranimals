Crea el projecte
----------------

$ mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=org.iesjoandaustria.animals -DartifactId=gestoranimals

Inicia repositori git
---------------------

Crea un repositori git a la nova carpeta. Haurà de gestionar com a
mínim:

* pom.xml

* src/

Afegeix a .gitignore els següents continguts:

    *~

Primer commit

Considera pujar el projecte a un github o bitbucket

